import 'package:covid_app/profile.dart';
import 'package:covid_app/question.dart';
import 'package:covid_app/vaccine.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MaterialApp(
    title: 'Covid 19',
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String name = '';
  String gender = 'M';
  int age = 0;
  var questionScore = 0.0;
  var vaccince = ['-', '-', '-'];

  @override
  void initState() {
    super.initState();
    _loadProfile();
    _loadQuestion();
  }

  Future<void> _loadQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    var strQuestionValue = prefs.getString('question_values') ??
        '[false, false, false, false, false, false, false, false, false, false, false]';
    var arrStrQuestionValue =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      questionScore = 0.0;
      for (var i = 0; i < arrStrQuestionValue.length; i++) {
        if (arrStrQuestionValue[i].trim() == 'true') {
          questionScore++;
        }
      }
      questionScore = 100 * questionScore / 11;
    });
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? '';
      gender = prefs.getString('gender') ?? 'M';
      age = prefs.getInt('age') ?? 0;
      vaccince = prefs.getStringList('vaccines') ?? ['-', '-', '-'];
    });
  }

  Future<void> _resetProfile() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('name');
    prefs.remove('gender');
    prefs.remove('age');
    prefs.remove('vaccine');
    _loadProfile();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Main')),
      body: ListView(
        children: [
          Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.person),
                  title: Text(name),
                  subtitle: Text(
                    'เพศ: $gender,อายุ: $age ปี',
                    style: TextStyle(color: Colors.black.withOpacity(0.6)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'อาการ : ${questionScore.toStringAsFixed(2)} %',
                    style: TextStyle(
                        fontSize: 24.0,
                        color: (questionScore > 30)
                            ? Colors.red.withOpacity(0.6)
                            : Colors.green.withOpacity(0.6)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'ได้รับการฉีด : ${vaccince.toString()}',
                    style: TextStyle(color: Colors.black.withOpacity(0.6)),
                  ),
                ),
                ButtonBar(
                  alignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                      onPressed: () async {
                        await _resetProfile();
                      },
                      child: const Text('Reset'),
                    ),
                  ],
                ),
              ],
            ),
          ),
          ListTile(
            title: Text('Profile'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ProfileWidget()));
              await _loadProfile();
            },
          ),
          ListTile(
            title: Text('Vaccine'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => VaccineWidget()));
              await _loadProfile();
            },
          ),
          ListTile(
            title: Text('Question'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => QuestionWidget()));
              await _loadQuestion();
            },
          )
        ],
      ),
    );
  }
}
