import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VaccineWidget extends StatefulWidget {
  VaccineWidget({Key? key}) : super(key: key);

  @override
  _VaccineWidgetState createState() => _VaccineWidgetState();
}

class _VaccineWidgetState extends State<VaccineWidget> {
  var vaccince = ['-', '-', '-'];

  @override
  void initState() {
    super.initState();
    _loadVaccinces();
  }

  _loadVaccinces() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      vaccince = prefs.getStringList('vaccines') ?? ['-', '-', '-'];
    });
  }

  _saveVaccinces() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setStringList('vaccines', vaccince);
    });
  }

  Widget _vaccinceComboBox(
      {required String title,
      required String value,
      ValueChanged<String?>? onChanged}) {
    return Row(
      children: [
        Text(title),
        Expanded(child: Container()),
        DropdownButton(
          items: [
            DropdownMenuItem(
              child: Text('-'),
              value: '-',
            ),
            DropdownMenuItem(
              child: Text('Pfizer'),
              value: 'Pfizer',
            ),
            DropdownMenuItem(
              child: Text('Johnson & Johnson'),
              value: 'Johnson & Johnson',
            ),
            DropdownMenuItem(
              child: Text('Novavax'),
              value: 'Novavax',
            ),
            DropdownMenuItem(
              child: Text('Sinopharm'),
              value: 'Sinopharm',
            ),
            DropdownMenuItem(
              child: Text('Sinovac'),
              value: 'Sinovac',
            )
          ],
          value: value,
          onChanged: onChanged,
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Vaccine')),
        body: ListView(
          children: [
            _vaccinceComboBox(
              title: 'เข็มที่ 1',
              value: vaccince[0],
              onChanged: (newValue) {
                setState(() {
                  vaccince[0] = newValue!;
                });
              },
            ),
            _vaccinceComboBox(
              title: 'เข็มที่ 2',
              value: vaccince[1],
              onChanged: (newValue) {
                setState(() {
                  vaccince[1] = newValue!;
                });
              },
            ),
            _vaccinceComboBox(
              title: 'เข็มที่ 3',
              value: vaccince[2],
              onChanged: (newValue) {
                setState(() {
                  vaccince[2] = newValue!;
                });
              },
            ),
            ElevatedButton(
              onPressed: () {
                _saveVaccinces();
                Navigator.pop(context);
              },
              child: const Text('Save'),
            )
          ],
        ));
  }
}
